package com.sample.elastic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleElasticSearchApplication {

	private static final String INPUT_ZIP_FILE = "C:\\xml.zip";
	private static final String OUTPUT_FOLDER = "C:\\outputxml";
	
	public static void main(String[] args) throws IOException, JSONException {
		//Step 1 : unzip the xml file and parse it to json object
		FileInputStream resourceInputStream = new FileInputStream(INPUT_ZIP_FILE);
		unZip(resourceInputStream, OUTPUT_FOLDER);
	
		String fileInString = new File(OUTPUT_FOLDER).toString();
		JSONObject json1 = XML.toJSONObject(fileInString);
		
		/*//if there are more documents iterate each line and append to stringBuilder and replace fileInString with largeFileString
		Scanner scnr = new Scanner(new File(OUTPUT_FOLDER));
		String largeFileString = null;
		while(scnr.hasNextLine()){
            String line = scnr.nextLine();
            largeFileString = new StringBuilder(largeFileString).append(line).toString();
		}
		scnr.close();*/
		
		//Step 2 : connect to elastic server with transport client 
		TransportAddress address = new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300);
		Settings settings = Settings.builder()
				.put("cluster.name", "elasticsearch")
				.put("client.transport.sniff", true)
				.build();
		@SuppressWarnings("resource")
		Client client = new PreBuiltTransportClient(settings).addTransportAddress(address);
		
		//Step 3 : Send the parsed data to create an index called 'elastic'
		IndexResponse response = client.prepareIndex("elastic", "search").setSource(json1, XContentType.JSON).get();

		// Step 4 : print out the Index name created
		String _index = response.getIndex();
		System.out.println(_index);

		client.close();
		System.exit(0);
	}
	
	public static void unZip(FileInputStream fis, String outputFolder) throws IOException {
        byte[] buffer = new byte[1024];
        File folder = new File(outputFolder);
        if (!folder.exists()) {
            folder.mkdir();
        }
        ZipInputStream zis = new ZipInputStream(fis);
        ZipEntry ze = zis.getNextEntry();
        while (ze != null) {
            String fileName = ze.getName();
            File newFile = new File(outputFolder + File.separator + fileName);
            new File(newFile.getParent()).mkdirs();
            FileOutputStream fos = new FileOutputStream(newFile);
            int len;
            while ((len = zis.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }
            fos.close();
            ze = zis.getNextEntry();
        }

        zis.closeEntry();
        zis.close();
    }

	
}
